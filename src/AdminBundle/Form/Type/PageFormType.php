<?php
namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder->add('title', TextType::class);
        $builder->add('slug', TextType::class);
        $builder->add('isActive', ChoiceType::class, array('choices' => array(
            'Активный' => true,
            'Неактивный' => false
        )));
        $builder->add('content', TextareaType::class, array('required' => false));
    }

    public function getBlockPrefix()
    {
        return 'app_page';
    }
}