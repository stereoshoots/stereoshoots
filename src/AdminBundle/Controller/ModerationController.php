<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Category;
use AppBundle\Entity\Activity;

class ModerationController extends Controller
{
    public function indexAction(Request $request)
    {	
	$em = $this->getDoctrine()->getManager();
	
        $photos = $em->getRepository('AppBundle:Photo')->findBy(array('isActive' => true, 'isModerated' => false, 'moderationDate' => null), array('creationDate' => 'ASC'));
	
	return $this->render('AdminBundle::moderation.html.twig', array('photos' => $photos));
    }
    
    public function photographyAction(Request $request)
    {	
	$em = $this->getDoctrine()->getManager();
	
	$category = $em->getRepository('AppBundle:Category')->findByName(array('photography'));

        $photos = $this->getNotModeratedPhotosByCategory($category);
	
	return $this->render('AdminBundle::moderation.html.twig', array('photos' => $photos));
    }
    
    public function paintingAction(Request $request)
    {	
	$em = $this->getDoctrine()->getManager();
	
	$category = $em->getRepository('AppBundle:Category')->findByName(array('painting'));
        
        $photos = $this->getNotModeratedPhotosByCategory($category);
	
	return $this->render('AdminBundle::moderation.html.twig', array('photos' => $photos));
    }
    
    public function threeDemensionalAction(Request $request)
    {	
	$em = $this->getDoctrine()->getManager();
	
	$category = $em->getRepository('AppBundle:Category')->findByName(array('3d'));
        
        $photos = $this->getNotModeratedPhotosByCategory($category);
	
	return $this->render('AdminBundle::moderation.html.twig', array('photos' => $photos));
    }
    
    public function getNotModeratedPhotosByCategory($category) {
        $em = $this->getDoctrine()->getManager();
        
	$categories = $em->getRepository('AppBundle:Category')->findByRootCategory($category);
        
	$photoRepository = $em->getRepository('AppBundle:Photo');
	
	$photosQuery = $photoRepository->createQueryBuilder('p')
		->where('p.category IN(:categories) AND p.isActive = :isActive AND p.isModerated = :isModerated AND p.moderationDate IS NULL')
                ->setParameter('categories', $categories)
		->setParameter('isActive', true)
		->setParameter('isModerated', false)
		->orderBy("p.creationDate", 'ASC')
		->getQuery();
	
	return $photosQuery->getResult();
    }
    
    public function moderateAction(Request $request, $photoId) {
        $em = $this->getDoctrine()->getManager();
	$action =  $request->request->get('action');
        $photo = $em->getRepository('AppBundle:Photo')->find($photoId);
        
        if ($request->request->get('rejectReason')) {
            $rejectReason =  $request->request->get('rejectReason');
        } else {
            $rejectReason = null;
        }
        
	$currentDate = new \DateTime();
	
        $systemUser = $em->getRepository('AppBundle:User')->findOneByUsername('emotionfolio');
        
	switch($action) {
	    case 'accept':
		$photo->setModerated(true);
		$photo->setModerationDate($currentDate);
                
                $activity = new Activity(Activity::TYPE_MODERATE_SUCCESS, $systemUser, $photo->getUser(), $photo);
		break;
	    case 'decline':
		$photo->setModerated(false);
		$photo->setActive(false);
		$photo->setModerationDate($currentDate);
                $photo->setRejectReason($rejectReason);
                
		$activity = new Activity(Activity::TYPE_MODERATE_REJECT, $systemUser, $photo->getUser(), $photo);
		break;
	    default:
		throw new Exception('Unexpected action: '.$action);
		break;
	}
        
        $em->persist($photo);
        $em->persist($activity);
	$em->flush();
	
	return new Response('success');
    }
}

