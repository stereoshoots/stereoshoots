<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Form\Type\EditPhotoFormType;

class PhotoController extends Controller
{
    public function editAction(Request $request, $photoId)
    {	
	$em = $this->getDoctrine()->getManager();
	$photo = $em->getRepository('AppBundle:Photo')->find($photoId);
        
	$form = $this->createForm(EditPhotoFormType::class, $photo);
	$form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->flush();
            
            return $this->redirectToRoute('admin');
        }
        
	return $this->render('AdminBundle::editPhoto.html.twig', array('form' => $form->createView(), 'photo' => $photo));
    }
}

