<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Page;
use AdminBundle\Form\Type\PageFormType;

class PageController extends Controller
{
    public function pagesAction(Request $request)
    {	
	$em = $this->getDoctrine()->getManager();
	$pages = $em->getRepository('AdminBundle:Page')->findAll();
	
	return $this->render('AdminBundle::pages.html.twig', array('pages' => $pages));
    }
    
    public function createAction(Request $request)
    {	
        $page = new Page();
        $page->setCreationDate(new \DateTime());
        
	$form = $this->createForm(PageFormType::class, $page);
        $form->handleRequest($request);
        
	if ($form->isValid()) {
            $page->setIsActive(true);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            
            return $this->redirectToRoute('admin_pages');
        }
        
	return $this->render('AdminBundle::createPage.html.twig', array('form' => $form->createView()));
    }
    
    public function deleteAction(Request $request, $pageId)
    {	
        $em = $this->getDoctrine()->getManager();
	$page = $em->getRepository('AdminBundle:Page')->find($pageId);
        $page->setIsActive(false);
        
        $em->persist($page);
        $em->flush();
	
        return $this->redirectToRoute('admin_pages');
    }
    
    public function editAction(Request $request, $pageId)
    {	
        $em = $this->getDoctrine()->getManager();
	$page = $em->getRepository('AdminBundle:Page')->find($pageId);
        
        $form = $this->createForm(PageFormType::class, $page);
        $form->handleRequest($request);
        
	if ($form->isValid()) {
            $em->flush();
            
            return $this->redirectToRoute('admin_pages');
        }
        
	return $this->render('AdminBundle::editPage.html.twig', array('page' => $page, 'form' => $form->createView()));
    }
}

