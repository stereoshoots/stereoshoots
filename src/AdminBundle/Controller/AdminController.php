<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\PhotoController;
use AppBundle\Controller\CommentController;

class AdminController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $allTimePhotos = $em->getRepository('AppBundle:Photo')->findBy(array('isModerated' => true, 'isActive' => true));
        $allTimeUsers = $em->getRepository('AppBundle:User')->findAll();
        $allTimeComments = $em->getRepository('AppBundle:Comment')->findBy(array('isActive' => true));

	return $this->render('AdminBundle::index.html.twig', array(
            'allTimePhotos' => $allTimePhotos,
            'allTimeUsers' => $allTimeUsers,
            'allTimeComments' => $allTimeComments
        ));
    }
}

