<?php
namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
            ->add('avatar', FileType::class, array('required' => false, 'data_class' => null))
            ->add('wallImage', FileType::class, array('required' => false, 'data_class' => null))
            ->add('about', TextareaType::class, array('required' => false))
            ->add('firstName', TextType::class, array('required' => false))
            ->add('lastName', TextType::class, array('required' => false))
            ->add('phone', TextType::class, array('required' => false))
            ->add('gender', ChoiceType::class, array('choices'  => array(
                'gender.not.specified' => 'not specifed',
                'gender.male' => 'male',
                'gender.female' => 'female'),
            ))
            ->add('birthDate', DateType::class, array('years' => range(date('Y') -100, date('Y'))))
            ->add('locationCountry', TextType::class, array('required' => false))
            ->add('locationCity', TextType::class, array('required' => false))
            ->remove('username')
            ->remove('email')
            ->remove('birthDate')
            ->remove('current_password');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
}