<?php
namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use AppBundle\Entity\File;
use AppBundle\Exception\FileException;
use AppBundle\Entity\Follower;
use AppBundle\Entity\Photo;
use AppBundle\Common\Template;
use AppBundle\Entity\User;

class ProfileController extends BaseController
{    
    public function indexAction(Request $request, $username)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager();

	if($request->request->get('offset'))
            $offset = $request->request->get('offset');
        else
            $offset = 0;

        if($authChecker->isGranted('ROLE_USER'))
            $user = $this->getUser();
	else
	    $user = null;
	
	$userManager = $this->container->get('fos_user.user_manager');
	$profileUser = $userManager->findUserByUsername($username);
	
	if(!$profileUser)
	    throw $this->createNotFoundException('Пользователь не найден');

        if($authChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if($user->getId() == $profileUser->getId())
                $photos = $em->getRepository('AppBundle:Photo')->findBy(array(
                    'user' => $profileUser,
                    'isActive' => true,
                ), array('creationDate' => 'DESC'), Photo::FLOW_PHOTOS_LIMIT, $offset);
            else
                $photos = $em->getRepository('AppBundle:Photo')->findBy(array(
                    'user' => $profileUser,
                    'isActive' => true,
                    'isModerated' => true,
                ), array('creationDate' => 'DESC'), Photo::FLOW_PHOTOS_LIMIT, $offset);
        }
        else
            $photos = $em->getRepository('AppBundle:Photo')->findBy(array(
                'user' => $profileUser,
                'isActive' => true,
                'isModerated' => true,
            ), array('creationDate' => 'DESC'), Photo::FLOW_PHOTOS_LIMIT, $offset);
        
        if($user) {
            Photo::checkIfPhotosAreLikedByUser($photos, $user, $em);
            User::checkIfUserIsFollowedByUser($profileUser, $user, $em);
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	if($offset)
            return $this->render($template->getTemplateByDevice('AppBundle', 'flowPhoto.html.twig'), array('photos' => $photos));
	
	return $this->render($template->getTemplateByDevice('AppBundle', 'profile.html.twig'), array(
            'user' => $profileUser,
            'photos' => $photos
        ));
    }
    
        
    public function followersAction(Request $request, $username)
    {	
	$em = $this->getDoctrine()->getManager();
	$userManager = $this->container->get('fos_user.user_manager');
	
        $loggedUser = $this->getUser();
	$user = $userManager->findUserByUsername($username);

	if(!$user)
	    throw $this->createNotFoundException('Пользователь не найден');
	
        if($request->request->get('offset'))
            $offset = $request->request->get('offset');
        else
            $offset = 0;
        
        $followers = $em->getRepository('AppBundle:Follower')->findBy(array('user' => $user, 'isActive' => 1), null, Follower::FOLLOWER_DISPLAY_LIMIT, $offset);
        
        foreach($followers as $key => $follower) {
            $follow = $em->getRepository('AppBundle:Follower')->findBy(array(
                'user' => $follower->getFollower(),
                'follower' => $loggedUser,
                'isActive' => true
            ));
            
            if($follow)
                $follower->getFollower()->setFollowed(true);
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'followers.html.twig'), array(
            'user' => $loggedUser,
            'followers' => $followers
        ));
    }
    
    public function followingAction(Request $request, $username)
    {
	$em = $this->getDoctrine()->getManager();
	$userManager = $this->container->get('fos_user.user_manager');
	
        $loggedUser = $this->getUser();
	$user = $userManager->findUserByUsername($username);
	
	if(!$user) {
	    throw $this->createNotFoundException('Пользователь не найден');
	}
        
        if($request->request->get('offset'))
            $offset = $request->request->get('offset');
        else
            $offset = 0;
        
        $followers = $em->getRepository('AppBundle:Follower')->findBy(array('follower' => $user, 'isActive' => 1), null, Follower::FOLLOWER_DISPLAY_LIMIT, $offset);
        
        foreach($followers as $key => $follower) {
            $follow = $em->getRepository('AppBundle:Follower')->findBy(array(
                'user' => $follower->getUser(),
                'follower' => $loggedUser,
                'isActive' => true
            ));
            
            if($follow)
                $follower->getUser()->setFollowed(true);
            
        }
       
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'following.html.twig'), array(
            'followers' => $followers
        ));
    }

    public function editAction(Request $request)
    {
	$user = $this->getUser();
	$avatar = $user->getAvatar();
        $wallImage = $user->getWallImage();
        
        $currentDate = new \DateTime();
        $salt = uniqid();
        
        if(!is_object($user) || !$user instanceof UserInterface)
            throw new AccessDeniedException('This user does not have access to this section.');
	
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if(null !== $event->getResponse())
            return $event->getResponse();
        
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();	
        $form->setData($user);
        $form->handleRequest($request);
        
        if($form->isValid()) { 
	    if(!empty($form['avatar']->getData())) {
		$extension = $form['avatar']->getData()->guessExtension();
                $avatarName = sha1($user->getId().$currentDate->format('d-m-Y H:i:s').$salt).'.'.$extension;
                
		if(!in_array($extension, File::$extensions))
		    throw new FileException('Недопустимое расширение', FileException::FILE_EXTENSION_DENIED);
		
		$user->setAvatar($avatarName);
		$form['avatar']->getData()->move(File::getAvatarUploadPath($user->getId()), $avatarName);
	    }
            else
                $user->setAvatar($avatar);
            
            if(!empty($form['wallImage']->getData())) {
                $extension = $form['wallImage']->getData()->guessExtension();
                $wallImageName = sha1($user->getId().$currentDate->format('d-m-Y H:i:s').$salt).'.'.$extension;
                
		if(!in_array($extension, File::$extensions))
		    throw new FileException('Недопустимое расширение', FileException::FILE_EXTENSION_DENIED);
		
		$user->setWallImage($wallImageName);
		$form['wallImage']->getData()->move(File::getWallImageUploadPath($user->getId()), $wallImageName);
	    }
            else
                $user->setWallImage($wallImage);
	    
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
	    
            $userManager->updateUser($user);

            if(null === $response = $event->getResponse()) {
                $url = $this->generateUrl('profile', array('username' => $user->getUsername()));
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            
            return $response;
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        return $this->render($template->getTemplateByDevice('AppBundle', 'edit.html.twig'), array('form' => $form->createView()));
    }
}