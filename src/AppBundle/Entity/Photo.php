<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="photos")
 */
class Photo
{    
   
    const   FLOW_PHOTOS_LIMIT = 15,
            OPTIMIZED_PHOTO_POSTFIX = '_opt';
            
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="photos")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="text")
     */
    protected $title;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\Column(type="text")
     */
    protected $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    protected $category;
    
    /**
     * @ORM\Column(name="creation_date", type="datetime")
     */
    protected $creationDate;
    
    /**
     * @ORM\Column(name="edit_date", type="datetime", nullable=true)
     */
    protected $editDate;
    
    /**
     * @ORM\Column(name="is_moderated", type="boolean")
     */
    protected $isModerated = false;
    
    /**
     * @ORM\Column(name="moderation_date", type="datetime", nullable=true)
     */
    protected $moderationDate;
    
    /**
     * @ORM\Column(name="reject_reason", type="text", nullable=true)
     */
    protected $rejectReason;
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;
    
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="photo")
     */
    protected $comments;
    
    /**
     * @ORM\OneToMany(targetEntity="Like", mappedBy="photo")
     * @ORM\OrderBy({"user" = "DESC"})
     */
    protected $likes;
    
    /*
     * Liked by logged user
     * 
     * @return boolean
     */
    protected $likedByUser = false;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Get category
     *
     * @return Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    /**
     * Get editDate
     *
     * @return \DateTime
     */
    public function getEditDate()
    {
        return $this->editDate;
    }
    
    /**
     * Get moderationDate
     *
     * @return \DateTime
     */
    public function getModerationDate()
    {
        return $this->moderationDate;
    }
    
    /**
     * Get likes
     *
     * @return Like $likes
     */
    public function getLikes()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        $criteria->orderBy(array('creationDate' => 'DESC'));
        
        return $this->likes->matching($criteria);
    }
    
    /**
     * Get comments
     *
     * @return Comment $comments
     */
    public function getComments()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        $criteria->orderBy(array('creationDate' => 'DESC'));

        return $this->comments->matching($criteria);
    }
    
    /*
     * Get liked by user
     */
    public function getLikedByUser()
    {
        return $this->likedByUser;
    }
    
    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Get is active
     *
     * @return integer
     */
    public function isActive()
    {
        return $this->isActive;
    }
    
    /**
     * Get is moderated
     *
     * @return integer
     */
    public function isModerated()
    {
        return $this->isModerated;
    }
    
    /*
     * Get image
     * 
     * @return string
     */
    public function getImage()
    {
        return $this->getWebDirectory().$this->getName();
    }
    
    /*
     * Get image directory 
     * 
     * @return string
     */
    public function getDirectory()
    {
        return __DIR__.'/../../../web/media/uploads/photos/'.$this->getUser()->getId().'/'.$this->creationDate->format('Y-m-d').'/';
    }
    
    /*
     * Get image web directory
     * 
     * @return string
     */
    public function getWebDirectory()
    {
        return '/media/uploads/photos/'.$this->getUser()->getId().'/'.$this->creationDate->format('Y-m-d').'/';
    }
    
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Get rejectReason
     *
     * @return string
     */
    public function getRejectReason()
    {
        return $this->rejectReason;
    }
    
    /**
     * Set user
     *
     * @param User $user
     *
     * @return Photo
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
    
    /**
     * Set title
     *
     * @param string $title
     *
     * @return Photo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Photo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Photo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
    }
    
    /**
     * Set editDate
     *
     * @param \DateTime $editDate
     */
    public function setEditDate(\DateTime $editDate)
    {
        $this->editDate = $editDate;
    }
    
    /**
     * Set moderationDate
     *
     * @param \DateTime $moderationDate
     */
    public function setModerationDate(\DateTime $moderationDate)
    {
        $this->moderationDate = $moderationDate;
    }
    
    /**
     * Set active
     */
    public function setActive($active)
    {
        $this->isActive = $active;
    }
    
    /**
     * Set category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
    
    /**
     * Set moderated
     */
    public function setModerated($moderated)
    {
        $this->isModerated = $moderated;
    }
    
    /**
     * Set rejectReason
     *
     * @param string $name
     *
     * @return Photo
     */
    public function setRejectReason($rejectReason)
    {
        $this->rejectReason = $rejectReason;
        
        return $this;
    }
    
    /**
     * Set liked by user
     */
    public function setLikedByUser($liked)
    {
        $this->likedByUser = $liked;
    }
    
    public function deleteImage() {
	return unlink($this->getDirectory().$this->getName());
    }
    
    public static function checkIfPhotosAreLikedByUser($photos, $user, $em) {
        foreach($photos as $key => $photo) {
            self::checkIfPhotoIsLikedByUser($photo, $user, $em);
        }
        
        return $photos;
    }
    
    public static function checkIfPhotoIsLikedByUser($photo, $user, $em)
    {
        $like = $em->getRepository('AppBundle:Like')->findOneBy(array(
            'photo' => $photo,
            'user' => $user,
            'isActive' => true
        ));

        if($like)
            $photo->setLikedByUser(true);

        return $photo;
    }
}
