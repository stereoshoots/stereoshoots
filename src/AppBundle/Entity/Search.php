<?php
namespace AppBundle\Entity;

class Search
{
    protected $phrase;

    /**
     * Get phrase
     *
     * @return string
     */
    public function getPhrase()
    {
        return $this->phrase;
    }
    
    /**
     * Set phrase
     *
     * @param string $phrase
     *
     * @return Search
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;

        return $this;
    }
}
