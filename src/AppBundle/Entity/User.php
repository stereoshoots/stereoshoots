<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    const DISCOVER_USERS_LIMIT = 15;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Assert\Regex(
     *     pattern     = "/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/i",
     * )
     */
    protected $username;
    
    /**
     * @ORM\Column(name="first_name", type="text", nullable=true)
     */
    protected $firstName;
    
    /**
     * @ORM\Column(name="last_name", type="text", nullable=true)
     */
    protected $lastName;
    
    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $phone;
    
    /**
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    protected $birthDate;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $gender;
    
    /**
     * @ORM\Column(name="location_country", type="text", nullable=true)
     */
    protected $locationCountry;
    
    /**
     * @ORM\Column(name="location_city", type="text", nullable=true)
     */
    protected $locationCity;

    /**
    * @ORM\Column( type="text", nullable=true)
    */
    protected $avatar;
    
    /**
    * @ORM\Column(name="wall_image", type="text", nullable=true)
    */
    protected $wallImage;
    
    /**
    * @ORM\Column( type="text", nullable=true)
    */
    protected $about;
    
    /**
     * @ORM\OneToMany(targetEntity="Follower", mappedBy="user")
     */
    protected $followers;
    
    /**
     * @ORM\OneToMany(targetEntity="Follower", mappedBy="follower")
     */
    protected $followings;
    
    /**
     * Is followed. Used when checking is followed by another user.
     */
    protected $isFollowed = false;
    
    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="user")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $photos;
    
    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="recipient")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $activity;
    
    /**
     * @ORM\Column(name="register_date", type="datetime", nullable=true)
     */
    protected $registerDate;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $mailing = true;

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }
    
    /**
     * Set mailing
     *
     * @param string $mailing
     *
     * @return User
     */
    public function setMailing($mailing)
    {
        $this->mailing = $mailing;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Get mailing
     *
     * @return string
     */
    public function getMailing()
    {
        return $this->mailing;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set locationCountry
     *
     * @param string $locationCountry
     *
     * @return User
     */
    public function setLocationCountry($locationCountry)
    {
        $this->locationCountry = $locationCountry;

        return $this;
    }

    /**
     * Get locationCountry
     *
     * @return string
     */
    public function getLocationCountry()
    {
        return $this->locationCountry;
    }

    /**
     * Set locationCity
     *
     * @param string $locationCity
     *
     * @return User
     */
    public function setLocationCity($locationCity)
    {
        $this->locationCity = $locationCity;

        return $this;
    }

    /**
     * Get locationCity
     *
     * @return string
     */
    public function getLocationCity()
    {
        return $this->locationCity;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set wallImage
     *
     * @param string $wallImage
     *
     * @return User
     */
    public function setWallImage($wallImage)
    {
        $this->wallImage = $wallImage;

        return $this;
    }

    /**
     * Get wallImage
     *
     * @return string
     */
    public function getWallImage()
    {
        return $this->wallImage;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }
    
    /**
     * Set isFollowed
     *
     * @param \Boolean $isFollowed
     *
     * @return User
     */
    public function setFollowed($isFollowed)
    {
        $this->isFollowed = $isFollowed;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Add follower
     *
     * @param \AppBundle\Entity\Follower $follower
     *
     * @return User
     */
    public function addFollower(\AppBundle\Entity\Follower $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \AppBundle\Entity\Follower $follower
     */
    public function removeFollower(\AppBundle\Entity\Follower $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        
        return $this->followers->matching($criteria);
    }

    /**
     * Add following
     *
     * @param \AppBundle\Entity\Follower $following
     *
     * @return User
     */
    public function addFollowing(\AppBundle\Entity\Follower $following)
    {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param \AppBundle\Entity\Follower $following
     */
    public function removeFollowing(\AppBundle\Entity\Follower $following)
    {
        $this->followings->removeElement($following);
    }

    /**
     * Get followings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowings()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        
        return $this->followings->matching($criteria);
    }

    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return User
     */
    public function addPhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        $criteria->where(Criteria::expr()->eq('isModerated', true));
        
        return $this->photos->matching($criteria);
    }
    
    /**
     * Get activity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivity()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        $criteria->orderBy(array('creationDate' => 'DESC'));
        $criteria->setMaxResults(Activity::MAX_RESULTS);

        return $this->activity->matching($criteria);
    }
    
    /**
     * is Followed
     *
     * @return bool
     */
    public function isFollowed()
    {
        return $this->isFollowed;
    }
    
    /*
     * Get user avatars path
     */
    public function getAvatarPath()
    {
        return '/media/uploads/avatars/'.$this->id.'/';
    }
    
    /*
     * Get user wall image path
     */
    public function getWallImagePath()
    {
        return '/media/uploads/wall/'.$this->id.'/';
    }
    
    public function getAvatarImageUrl()
    {
	return $this->getAvatarPath().$this->avatar;
    }
    
    public function getWallImageUrl()
    {
	return $this->getWallImagePath().$this->wallImage;
    }
    
    public function checkIfUsersAreFollowedByUser($users, $loggedUser, $em)
    {
        foreach($users as $key => $user) {
	    self::checkIfUserIsFollowedByUser($user, $loggedUser, $em);
        }
        
        return $users;
    }
    
    public static function checkIfUserIsFollowedByUser($user, $loggedUser, $em)
    {
        $follower = $em->getRepository('AppBundle:Follower')->findBy(array(
            'user' => $user,
            'follower' => $loggedUser,
            'isActive' => true
        ));

        if($follower)
            $user->setFollowed(true);
        
        return $user;
    }
}