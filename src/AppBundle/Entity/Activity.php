<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="activity")
 */
class Activity
{
    const
            TYPE_FOLLOW = 1,
            TYPE_LIKE = 2,
            TYPE_COMMENT = 3,
            TYPE_MODERATE_SUCCESS = 4,
            TYPE_MODERATE_REJECT = 5,
            
            MAX_RESULTS = 15;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $type;
    
    /**
     * @ORM\ManyToOne(targetEntity="Photo")
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id", nullable=true)
     */
    protected $photo;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $recipient;
    
    /**
     * @ORM\Column(name="creation_date", type="datetime")
     */
    protected $creationDate;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $seen;
    
    /**
     * @ORM\Column(name="seen_date", type="datetime", nullable=true)
     */
    protected $seenDate;
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;
    
    public function __construct($type, $user, $recipient, $photo = null)
    {
        $this->type = $type;
        $this->user = $user;
        $this->recipient = $recipient;
        $this->creationDate = new \DateTime();
        
        if($photo)
            $this->photo = $photo;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Activity
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Activity
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set seen
     *
     * @param boolean $seen
     *
     * @return Activity
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return boolean
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * Set seenDate
     *
     * @param \DateTime $seenDate
     *
     * @return Activity
     */
    public function setSeenDate($seenDate)
    {
        $this->seenDate = $seenDate;

        return $this;
    }

    /**
     * Get seenDate
     *
     * @return \DateTime
     */
    public function getSeenDate()
    {
        return $this->seenDate;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Activity
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Activity
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set recipient
     *
     * @param \AppBundle\Entity\User $recipient
     *
     * @return Activity
     */
    public function setRecipient(\AppBundle\Entity\User $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \AppBundle\Entity\User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return Activity
     */
    public function setPhoto(\AppBundle\Entity\Photo $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \AppBundle\Entity\Photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
