<?php
namespace AppBundle\Entity;

use AppBundle\Entity\Photo;
use AppBundle\Exception\PhotoEffectException;

class PhotoEffect
{
 
    public
            $photo,
            $image,
            $imageName,
            $photoName,
            $photoExtension,
            $output,
            $images = array(),
            $prefix = 'IMG';
    private 
            $width,
            $height,
            $tmp;
 
    public function __construct(Photo $photo)
    {
        if(file_exists($photo->getDirectory().$photo->getName())) {
            $this->photo = $photo;
            $this->image = $photo->getDirectory().$this->photo->getName();
            list($this->width, $this->height) = getimagesize($this->image);

            list($this->photoName, $this->photoExtension) = explode('.', $this->photo->getName());
        }
        else
            throw new PhotoEffectException('Photo not found.', PhotoEffectException::PHOTO_NOT_FOUND);
    }
    
    public function getAll() {
        foreach(array('gotham', 'toaster', 'nashville', 'lomo', 'kelvin') as $method) {
            $this->$method();
            $this->images[] = $this->imageName;
        }
        
        return $this->images;
    }
 
    public function tempfile()
    {
        $this->tmp = $this->photo->getDirectory().$this->prefix.uniqid();
        copy($this->image, $this->tmp);
    }
 
    public function output()
    {
        rename($this->tmp, $this->output);
    }
 
    public function execute($command)
    {
        $command = str_replace(array("\n", "'"), array('', '"'), $command);
        $command = escapeshellcmd($command);

        exec($command);
    }

    public function colortone($input, $color, $level, $type = 0)
    {
        $args[0] = $level;
        $args[1] = 100 - $level;
        $negate = $type == 0? '-negate': '';
 
        $this->execute("convert
        {$input}
        ( -clone 0 -fill '$color' -colorize 100% )
        ( -clone 0 -colorspace gray $negate )
        -compose blend -define compose:args=$args[0],$args[1] -composite
        {$input}");
    }
 
    public function border($input, $color = 'black', $width = 20)
    {
        $this->execute("convert $input -bordercolor $color -border {$width}x{$width} $input");
    }
 
    public function frame($input, $frame)
    {
        $this->execute("convert $input ( '$frame' -resize {$this->width}x{$this->height}! -unsharp 1.5×1.0+1.5+0.02 ) -flatten $input");
    }
 
    public function vignette($input, $color_1 = 'none', $color_2 = 'black', $crop_factor = 1.5)
    {
        $crop_x = floor($this->width * $crop_factor);
        $crop_y = floor($this->height * $crop_factor);
 
        $this->execute("convert
        ( {$input} )
        ( -size {$crop_x}x{$crop_y}
        radial-gradient:$color_1-$color_2
        -gravity center -crop {$this->width}x{$this->height}+0+0 +repage )
        -compose multiply -flatten
        {$input}");
    }
    
    public function gotham()
    {
        $this->tempfile();
        $this->execute("convert $this->tmp -modulate 120,10,100 -fill '#222b6d' -colorize 20 -gamma 0.5 -contrast -contrast $this->tmp");
        $this->border($this->tmp);
        
        $this->imageName = $this->photoName.'_gotham.'.$this->photoExtension;
        
        $this->output = $this->photo->getDirectory().$this->imageName;
        $this->output();
    }
    
    public function toaster()
    {
        $this->tempfile();
        $this->colortone($this->tmp, '#330000', 100, 0);

        $this->execute("convert $this->tmp -modulate 150,80,100 -gamma 1.2 -contrast -contrast $this->tmp");

        $this->vignette($this->tmp, 'none', 'LavenderBlush3');
        $this->vignette($this->tmp, '#ff9966', 'none');

        $this->imageName = $this->photoName.'_toaster.'.$this->photoExtension;
        $this->output = $this->photo->getDirectory().$this->imageName;
        $this->output();
    }
    
    public function nashville()
    {
        $this->tempfile();

        $this->colortone($this->tmp, '#222b6d', 100, 0);
        $this->colortone($this->tmp, '#f7daae', 100, 1);

        $this->execute("convert $this->tmp -contrast -modulate 100,150,100 -auto-gamma $this->tmp");
        $this->frame($this->tmp, __FUNCTION__);

        $this->imageName = $this->photoName.'_nashville.'.$this->photoExtension;
        $this->output = $this->photo->getDirectory().$this->imageName;
        $this->output();
    }
    
    public function lomo()
    {
        $this->tempfile();

        $command = "convert {$this->tmp} -channel R -level 33% -channel G -level 33% $this->tmp";

        $this->execute($command);
        $this->vignette($this->tmp);
        
        $this->imageName = $this->photoName.'_lomo.'.$this->photoExtension;
        $this->output = $this->photo->getDirectory().$this->imageName;
        $this->output();
    }
    
    public function kelvin()
    {
        $this->tempfile();

        $this->execute("convert
        ( $this->tmp -auto-gamma -modulate 120,50,100 )
        ( -size {$this->width}x{$this->height} -fill 'rgba(255,153,0,0.5)' -draw 'rectangle 0,0 {$this->width},{$this->height}' )
        -compose multiply
        $this->tmp");
        $this->frame($this->tmp, __FUNCTION__);

        $this->imagesName = $this->photoName.'_kelvin.'.$this->photoExtension;
        $this->output = $this->photo->getDirectory().$this->imagesName;
        $this->output();
    }
}
