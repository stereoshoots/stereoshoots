<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    protected $rootCategory;
    
    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rootCategory
     *
     * @param \AppBundle\Entity\Category $rootCategory
     *
     * @return Category
     */
    public function setRootCategory(\AppBundle\Entity\Category $rootCategory = null)
    {
        $this->rootCategory = $rootCategory;

        return $this;
    }

    /**
     * Get rootCategory
     *
     * @return \AppBundle\Entity\Category
     */
    public function getRootCategory()
    {
        return $this->rootCategory;
    }
}
