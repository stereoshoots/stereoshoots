<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="first_name", type="text", nullable=true)
     */
    protected $firstName;
    
    /**
     * @ORM\Column(name="last_name", type="text", nullable=true)
     */
    protected $lastName;
    
    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $phone;
    
    /**
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    protected $birthDate;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $gender;
    
    /**
     * @ORM\Column(name="location_country", type="text", nullable=true)
     */
    protected $locationCountry;
    
    /**
     * @ORM\Column(name="location_city", type="text", nullable=true)
     */
    protected $locationCity;

    /**
    * @ORM\Column( type="text", nullable=true)
    */
    protected $avatar;
    
    /**
    * @ORM\Column(name="wall_image", type="text", nullable=true)
    */
    protected $wallImage;
    
    /**
    * @ORM\Column( type="text", nullable=true)
    */
    protected $about;
    
    /**
     * @ORM\OneToMany(targetEntity="Follower", mappedBy="user")
     */
    protected $followers;
    
    /**
     * @ORM\OneToMany(targetEntity="Follower", mappedBy="follower")
     */
    protected $followings;
    
    /**
     * Is followed. Used when checking is followed by another user.
     */
    protected $isFollowed = false;
    
    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="user")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $photos;
    
    /**
     * @ORM\Column(name="register_date", type="datetime")
     */
    protected $registerDate;
}