<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Chumper\Zipper\Zipper;
use AppBundle\Common\Sign;

class BackupCommand extends ContainerAwareCommand
{
    const
            DATABASE_PREFIX = '_db',
            BACKUP_FULL_TYPE = '_full',
            BACKUP_INCREMENT_TYPE = '_increment',
            FOLDER_NAME_REGEXP = '/\d{4}-\d{2}-\d{2}$/';
    
    protected 
            $date = null,
            $day = null,
            $backupName = null,
            $mediaFolderName = 'media',
            $databaseFolderName = 'database',
            $backupDir = __DIR__.'/../../../backup/',
            $uploadsDir = __DIR__.'/../../../web/media/uploads/';

    protected function configure()
    {
        $this
            ->setName('backup:create')
            ->setDescription('Create Backup');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('start');
        
        $this->setDate(new \DateTime());

        switch($this->getDay()) {
            case 'Monday':
                $this->setBackupName($this->getDate()->format('Y_m_d').self::BACKUP_FULL_TYPE);
                $this->createBackup();
                break;
            case 'Tuesday':
            case 'Wednesday':
            case 'Thursday':
            case 'Friday':
            case 'Saturday':
            case 'Sunday':
                $this->setBackupName($this->getDate()->format('Y_m_d').self::BACKUP_INCREMENT_TYPE);
                $this->createBackup(true);
                break;
            default:
                throw new Exception('Unexpected day: '.$day);
        }

        $output->writeln('done');
    }
    
    public function createBackup($incremental = false) {
        if($incremental)
            $this->copyDirectory($this->getUploadsDir(), $this->getMediaFolderDir(), true);
        else
            $this->copyDirectory($this->getUploadsDir(), $this->getMediaFolderDir());
        
        $this->createDatabaseDump();
        
        $this->createArchive($this->getBackupFolderDir().'/', $this->getBackupFolderDir().'.zip');
        
        $fs = new Filesystem();
        $fs->remove('backup/'.$this->getBackupName());
    }
    
    public function createArchive($source, $destination) {
        $zipper = new Zipper();
        $zipper->make('backup/'.$this->getBackupName().'.zip')->add('backup/'.$this->getBackupName().'/');
    }
    
    public function copyDirectory($source, $destination, $incremental = false) {
        if(is_dir($source)) {
            mkdir($destination, 0775, true);
            
            $directory = dir($source);
            
            while(false !== ($readDirectory = $directory->read())) {
                if($readDirectory == '.' || $readDirectory == '..')
                    continue;

                $pathDir = $source.'/'.$readDirectory;
                if(is_dir($pathDir)) {
                    if($incremental) {
                        if(preg_match(self::FOLDER_NAME_REGEXP, $pathDir, $matches)) {
                            $date = $matches[0];
                            $startDate = $this->getDateByDay('Monday');
                            $folderDate = new \DateTime($date);
                            if($folderDate < $startDate) {
                               continue;
                            }
                        }
                    }
                    
                    if($incremental)
                        $this->copyDirectory($pathDir, $destination.'/'.$readDirectory, true);
                    else
                        $this->copyDirectory($pathDir, $destination.'/'.$readDirectory);
                    
                    continue;
                }
                
                copy($pathDir, $destination.'/'.$readDirectory );
            }

            $directory->close();
        }
        else
            copy($source, $destination);
    }
    
    public function createDatabaseDump() {
        $dump = shell_exec('pg_dump -U postgres -w -d emotionfolio --inserts');
        
        $fs = new Filesystem();
        $fs->dumpFile($this->getDatabaseFolderDir().'/dump.sql', $dump);
    }
    
    public function getBackupDir() {
        return $this->backupDir;
    }
    
    public function getUploadsDir() {
        return $this->uploadsDir;
    }
    
    public function getDate() {
        return $this->date;
    }
    
    public function getDay() {
        return $this->date->format('l');
    }
    
    public function getDateByDay($day) {
        
        $days = ['Monday' => 1, 'Tuesday' => 2, 'Wednesday' => 3, 'Thursday' => 4, 'Friday' => 5, 'Saturday' => 6, 'Sunday' => 7];

        $today = new \DateTime();
        $today->setISODate((int)$today->format('o'), (int)$today->format('W'), $days[ucfirst($day)]);
        
        return $today;
    }
    
    public function getBackupName() {
        return $this->backupName;
    }
    
    public function getMediaFolderName() {
        return $this->mediaFolderName;
    }
    
    public function getDatabaseFolderName() {
        return $this->databaseFolderName;
    }
    
    public function getBackupFolderDir() {
        return $this->getBackupDir().$this->getBackupName();
    }
    
    public function getDatabaseFolderDir() {
        return $this->getBackupFolderDir().'/'.$this->getDatabaseFolderName();
    }
    
    public function getMediaFolderDir() {
        return $this->getBackupFolderDir().'/'.$this->getMediaFolderName();
    }
    
    public function setBackupName($name) {
        $this->backupName = $name;
    }
    
    public function setDate(\DateTime $date) {
        $this->date = $date;
    }
}