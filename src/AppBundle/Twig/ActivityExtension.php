<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManager; 
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ActivityExtension extends \Twig_Extension
{
    protected $user;
    
    protected $em;
    
    public function __construct(EntityManager $em, TokenStorage $tokenStorage)
    {
        if(!empty($tokenStorage->getToken()))
            $this->user = $tokenStorage->getToken()->getUser();
        $this->em = $em;
    }
    
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('checkActivity', array($this, 'checkActivityFunction')),
        );
    }

    public function checkActivityFunction()
    {
        if(!$this->user)
            return false;
        
        $query = $this->em->createQuery("
	    SELECT a
		FROM AppBundle:Activity a
		   WHERE a.isActive = :isActive
                        AND a.recipient = :recipient
                        AND a.user != :recipient
                        AND a.seen IS NULL
                        AND a.seenDate IS NULL
	")
        ->setParameters(array(
            'isActive' => true,
            'recipient' => $this->user
	));
        
        //TODO
        //Remove this count(), instead put COUNT on db level
        return count($query->getResult());
    }

    public function getName()
    {
        return 'app_activity_extension';
    }
}