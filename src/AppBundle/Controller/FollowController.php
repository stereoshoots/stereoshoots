<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Follower;
use AppBundle\Exception\FollowException;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Activity;

class FollowController extends Controller
{ 
    public function subscribeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
	
        $userId = $request->request->get('userId');
	$followerId = $request->request->get('followerId');
	
        $user = $em->getRepository('AppBundle:User')->find($userId);
	$followerUser = $em->getRepository('AppBundle:User')->find($followerId);
	
	if (!$user || !$followerUser) {
	    return new Response('user not exist');
        }
        
        $follower = $em->getRepository('AppBundle:Follower')->findOneBy(array(
            'user' => $user,
            'follower' => $followerUser
        ));
	
        $activity = $em->getRepository('AppBundle:Activity')->findOneBy(
            array('user' => $followerUser, 'type' => Activity::TYPE_FOLLOW, 'recipient' => $user)
        );
        
	if (!$follower) {
	    $follower = new Follower();
	    $follower->setUser($user);
	    $follower->setFollower($followerUser);
        } else {
            $follower->setActive(true);
        }
        
        if(!$activity) {
            $activity = new Activity(Activity::TYPE_FOLLOW, $followerUser, $user);
        } else {
            $activity->setIsActive(true);
        }
        
	$em->persist($follower);
        $em->persist($activity);
	$em->flush();
	
	return new Response('subscribed');
    }
    
    public function unsubscribeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
	$userId = $request->request->get('userId');
        $followerId = $request->request->get('followerId');

	$user = $em->getRepository('AppBundle:User')->find($userId);
	$followerUser = $em->getRepository('AppBundle:User')->find($followerId);
	
	if (!$user || !$followerUser) {
	    return new Response('user not exist');
        }
        
	$follower = $em->getRepository('AppBundle:Follower')->findOneBy(array(
            'user' => $user,
            'follower' => $followerUser
        ));
        
	if (!$follower) {
	    return new Response('system error');
        }
        
	$follower->setActive(false);
        
        $activity = $em->getRepository('AppBundle:Activity')->findOneBy(
            array('user' => $followerUser, 'type' => Activity::TYPE_FOLLOW, 'recipient' => $user)
        );
        
        if ($activity) {
            $activity->setIsActive(false);
            $em->persist($activity);
        }
        
	$em->persist($follower);
	$em->flush();
	
	return new Response('unsubscribed');
    }
}

