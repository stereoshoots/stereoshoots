<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Page;
use AppBundle\Common\Template;

class PageController extends Controller
{
    public function showAction(Request $request, $slug)
    {	
	$em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AdminBundle:Page')->findOneBy(array('slug' => $slug));
        
        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }
        
        if (!$page->getIsActive()) {
            return $this->redirectToRoute('index');
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'page.html.twig'), array('page' => $page));
    }
}

