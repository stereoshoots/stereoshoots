<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Template;

class SettingController extends Controller
{ 
    public function indexAction(Request $request)
    {	
        if(!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = null;
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'settings.html.twig'), array('referer' => $referer));
    }
}

