<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Common\Template;

class ActivityController extends Controller
{ 
    public function indexAction(Request $request)
    {	
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        $this->updateActivities($user, $em);
        
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = null;
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'activity.html.twig'), array('referer' => $referer));
    }
    
    
    
    public function updateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        if (!$request->request->get('userId')) {
            return new Response('not enough parameters');
        }
        
        $userId = $request->request->get('userId');     
        $user = $em->getRepository('AppBundle:User')->find($userId);

	if (!$user) {
	    return new Response('user not exist');
        }
        
        $this->updateActivities($user, $em);
        
        return new Response('updated');
    }
    
    public function updateActivities($user, $em) {
        $criterias = array(
            'recipient' => $user,
            'seen' => null,
            'seenDate' => null,
            'isActive' => true
        );
        
        $activities = $em->getRepository('AppBundle:Activity')->findBy($criterias);
        
        foreach ($activities as $activity) {
            $activity->setSeen(true);
            $activity->setSeenDate(new \DateTime());
        }

        $em->flush();
        
        return $activities;
    }
}

