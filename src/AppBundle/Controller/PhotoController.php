<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Follower;
use AppBundle\Exception\FollowException;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Photo;
use AppBundle\Entity\Comment;
use AppBundle\Entity\File;
use AppBundle\Exception\PhotoException;
use AppBundle\Exception\FileException;
use AppBundle\Exception\CategoryException;
use AppBundle\Form\Type\CommentFormType;
use AppBundle\Form\Type\UploadFormType;
use AppBundle\Form\Type\EditPhotoFormType;
use AppBundle\Common\PhotoCompressor;
use AppBundle\Common\Template;
use AppBundle\Entity\User;

class PhotoController extends Controller
{ 
    public function indexAction(Request $request, $id)
    {
        $authChecker = $this->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager();
        
        $photo = $em->getRepository('AppBundle:Photo')->find($id);

        if (!$photo || !$photo->isActive()) {
	    throw $this->createNotFoundException('Photo not found');
        }
        
        $form = $this->createForm(CommentFormType::class, new Comment());
        
        if ($authChecker->isGranted('ROLE_USER')) {
            $loggedUser = $this->getUser();
            
            Photo::checkIfPhotoIsLikedByUser($photo, $loggedUser, $em);
            User::checkIfUserIsFollowedByUser($photo->getUser(), $loggedUser, $em);
        }
        
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = null;
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        return $this->render($template->getTemplateByDevice('AppBundle', 'photo.html.twig'), array(
	    'photo' => $photo,
	    'form' => $form->createView(),
            'referer' => $referer
	));
    }
    
    public function uploadAction(Request $request)
    {
	$em = $this->getDoctrine()->getManager();
	
	$user = $this->getUser();
	$photo = new Photo();

	$form = $this->createForm(UploadFormType::class, $photo);
	$form->handleRequest($request);

	if ($form->isValid()) {
	    $currentDate = new \DateTime();
	    $salt = uniqid();
	    
	    $extension = $form['name']->getData()->guessExtension();
	    
	    if (!in_array($extension, File::$extensions)) {
		throw new FileException('Недопустимое расширение', FileException::FILE_EXTENSION_DENIED);
            }
            
	    $photo->setUser($user);
	    $photo->setCreationDate($currentDate);
	    
	    $uploadPath = $photo->getDirectory();
	    $photoName = sha1($user->getId().$currentDate->format('d-m-Y H:i:s').$salt).'.'.$extension;
	    $optimizedPhotoName = sha1($user->getId().$currentDate->format('d-m-Y H:i:s').$salt).Photo::OPTIMIZED_PHOTO_POSTFIX.'.'.$extension;
	    
            $photo->setName($optimizedPhotoName);
	    $form['name']->getData()->move($uploadPath, $photoName);
	    
            $photoCompressor = new PhotoCompressor($uploadPath.$photoName);
            $photoCompressor->compress();

            $photoCompressor->save($uploadPath.$optimizedPhotoName);
            
            $category = $em->getRepository('AppBundle:Category')->find($form['category']->getData());
            $photo->setCategory($category);
            
	    $em->persist($photo);
	    $em->flush();
	    
            return $this->redirectToRoute('profile', array('username' => $user->getUsername()));
	}
	
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'upload.html.twig'), array(
	    'form' => $form->createView(),
	));
    }
    
    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
	$user = $this->getUser();
        
	$photo = $em->getRepository('AppBundle:Photo')->find($id);
	
        if ($user->getId() != $photo->getUser()->getId()) {
            throw new PhotoException('Photo is not yours', PhotoException::PHOTO_IS_NOT_YOURS);
        }
        
	$form = $this->createForm(EditPhotoFormType::class, $photo);
	$form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->flush();
            
            return $this->redirectToRoute('profile', array('username' => $photo->getUser()->getUsername()));
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'editPhoto.html.twig'), array('form' => $form->createView(), 'photo' => $photo));
    }
    
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        $photo = $em->getRepository('AppBundle:Photo')->find($id);
        
        if ($user->getId() != $photo->getUser()->getId()) {
            throw new PhotoException('Photo is not yours.', PhotoException::PHOTO_IS_NOT_YOURS);
        }
        
        $photo->setActive(false);
        
        $em->flush();
        
        return $this->redirectToRoute('profile', array('username' => $user->getUsername()));
    }
    
    public function pendingAction(Request $request)
    {
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        return $this->render($template->getTemplateByDevice('AppBundle', 'pending.html.twig'));
    }
    
    public function commentsAction(Request $request, $id)
    {
        $authChecker = $this->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager();
        
        if ($authChecker->isGranted('ROLE_USER')) {
            $user = $this->getUser();
        } else {
	    $user = null;
        }
        
        $photo = $em->getRepository('AppBundle:Photo')->find($id);

        if (!$photo || !$photo->isActive()) {
	    throw $this->createNotFoundException('Photo not found');
        }
        
        $form = $this->createForm(CommentFormType::class, new Comment());
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = null;
        }
        
        return $this->render($template->getTemplateByDevice('AppBundle', 'comments.html.twig'), array('photo' => $photo, 'referer' => $referer, 'form' => $form->createView()));
    }
}

