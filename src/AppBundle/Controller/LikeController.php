<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Like;
use AppBundle\Entity\Activity;

class LikeController extends Controller
{
    public function createAction(Request $request)
    {	
        $em = $this->getDoctrine()->getManager();
        
	$userId = $request->request->get('userId');
        $photoId = $request->request->get('photoId');
        $user = $em->getRepository('AppBundle:User')->find($userId);
	$photo = $em->getRepository('AppBundle:Photo')->find($photoId);
        
        if (!$user || !$photo) {
            return new Response('Not enough parameters');
        }
        
        $like = $em->getRepository('AppBundle:Like')->findOneBy(
            array('user' => $user, 'photo' => $photo)
        );
        
        $activity = $em->getRepository('AppBundle:Activity')->findOneBy(
            array('user' => $user, 'type' => Activity::TYPE_LIKE, 'photo' => $photo)
        );
        
        if (!$like) {
	    $like = new Like($user, $photo);
        } else {
            $like->setActive(true);
        }
        
        if (!$activity) {
            $activity = new Activity(Activity::TYPE_LIKE, $user, $photo->getUser(), $photo);
        } else {
            $activity->setIsActive(true);
        }
        
        $em->persist($like);
        $em->persist($activity);
	$em->flush();
         
        return new Response('success');
    }
    
    public function deleteAction(Request $request)
    {	
        $em = $this->getDoctrine()->getManager();
	$userId = $request->request->get('userId');
        $photoId = $request->request->get('photoId');
	
        $user = $em->getRepository('AppBundle:User')->find($userId);
	$photo = $em->getRepository('AppBundle:Photo')->find($photoId);
        
        if (!$user || !$photo) {
            return new Response('Not enough parameters');
        }
        
        $like = $em->getRepository('AppBundle:Like')->findOneBy(
            array('user' => $user, 'photo' => $photo)
        );
        
        $activity = $em->getRepository('AppBundle:Activity')->findOneBy(
            array('user' => $user, 'type' => Activity::TYPE_LIKE, 'photo' => $photo)
        );
        
        if ($like) {
	    $like->setActive(false);
            
            $em->persist($like);
        }
        
        if ($activity) {
            $activity->setIsActive(false);
            
            $em->persist($activity);
        }
        
	$em->flush();
         
        return new Response('success');
    }
}