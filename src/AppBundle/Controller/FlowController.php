<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Like;
use AppBundle\Entity\Photo;
use AppBundle\Entity\Category;
use AppBundle\Common\Template;

class FlowController extends Controller
{
            
    public function indexAction(Request $request)
    {	
        $authChecker = $this->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
	$emptyFlow = false;
	
	if($request->request->get('offset')) {
            $offset = $request->request->get('offset');
        } else {
            $offset = 0;
        }
        
	$photos = $this->getUserFlow($offset);
	
	if (!$photos) { 
	    $emptyFlow = true;
	    
	    $photos = $this->createFlow($offset, $user);
	}
        
        if ($authChecker->isGranted('ROLE_USER')) {
            Photo::checkIfPhotosAreLikedByUser($photos, $user, $em);
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        if ($offset) {
            return $this->render($template->getTemplateByDevice('AppBundle', 'flowPhoto.html.twig'), array('photos' => $photos));
        }
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'flow.html.twig'), array('photos' => $photos, 'emptyFlow' => $emptyFlow));
    }
    
    public function photographyAction(Request $request, $subCategory = null)
    {	
        $em = $this->getDoctrine()->getManager();
        $category = null;
        
        if ($request->request->get('offset')) {
            $offset = $request->request->get('offset');
        } else {
            $offset = 0;
        }
        
        if ($subCategory) {
            $category = $em->getRepository('AppBundle:Category')->findOneByName($subCategory);

            $photos = $this->getPopularPhotosByCategory($em, $category, $offset);
        } else {
            $photos = $this->getPopularPhotographies($em, $offset);
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        if ($offset) {
            return $this->render($template->getTemplateByDevice('AppBundle', 'flowPhoto.html.twig'), array('photos' => $photos));
        }
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'photography.html.twig'), array('photos' => $photos, 'category' => $category));
    }
    
    public function paintingAction(Request $request, $subCategory = null)
    {	
        $em = $this->getDoctrine()->getManager();
        $category = null;
        
        if ($request->request->get('offset')) {
            $offset = $request->request->get('offset');
        } else {
            $offset = 0;
        }
        
        if ($subCategory) {
            $category = $em->getRepository('AppBundle:Category')->findOneByName($subCategory);

            $photos = $this->getPopularPhotosByCategory($em, $category, $offset);
        } else {
            $photos = $this->getPopularPaintings($em, $offset);
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        if ($offset) {
            return $this->render($template->getTemplateByDevice('AppBundle', 'flowPhoto.html.twig'), array('photos' => $photos));
        }
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'painting.html.twig'), array('photos' => $photos, 'category' => $category));
    }
    
    public function threeDimensionalAction(Request $request, $subCategory = null)
    {	
        $em = $this->getDoctrine()->getManager();
        $category = null;
        
        if ($request->request->get('offset')) {
            $offset = $request->request->get('offset');
        } else {
            $offset = 0;
        }
        
        if ($subCategory) {
            $category = $em->getRepository('AppBundle:Category')->findOneByName($subCategory);

            $photos = $this->getPopularPhotosByCategory($em, $category, $offset);
        } else {
            $photos = $this->getPopular3dWorks($em, $offset);
        }
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        if($offset) {
            return $this->render($template->getTemplateByDevice('AppBundle', 'flowPhoto.html.twig'), array('photos' => $photos));
        }
        
	return $this->render($template->getTemplateByDevice('AppBundle', '3d.html.twig'), array('photos' => $photos, 'category' => $category));
    }
    
    public function getPopularPhotographies($em, $offset = 0) {
        $rootCategory = $em->getRepository('AppBundle:Category')->findOneByName('photography');
	$category = $em->getRepository('AppBundle:Category')->findByRootCategory($rootCategory);
        
	return $this->getPopularPhotosByCategory($em, $category, $offset);
    }
    
    public function getPopularPaintings($em, $offset = 0) {
	$rootCategory = $em->getRepository('AppBundle:Category')->findOneByName('painting');
	$category = $em->getRepository('AppBundle:Category')->findByRootCategory($rootCategory);
	
	return $this->getPopularPhotosByCategory($em, $category, $offset);
    }
    
    public function getPopular3dWorks($em, $offset = 0) {
	$rootCategory = $em->getRepository('AppBundle:Category')->findOneByName('3d');
	$category = $em->getRepository('AppBundle:Category')->findByRootCategory($rootCategory);
	
	return $this->getPopularPhotosByCategory($em, $category, $offset);
    }
    
    public function getPopularPhotosByCategory($em, $category, $offset) {
        $query = $em->createQuery("
	    SELECT p
		FROM AppBundle:Photo p
                JOIN AppBundle:Like l  WITH p = l.photo
		   WHERE p.isModerated = :isModerated
			AND p.isActive = :isActive
                        AND p.category IN(:categories)
                        AND p.creationDate <= :begin
                        AND p.creationDate >= :end
                        GROUP BY p.id
			ORDER BY p.moderationDate DESC
	")
        ->setParameters(array(
	    'isModerated' => true,
	    'isActive' => true,
            'categories' => $category,
            'begin' => new \DateTime('now'),
            'end' => new \DateTime('-60 days')
	))
        ->setMaxResults(Photo::FLOW_PHOTOS_LIMIT)
        ->setFirstResult($offset);
        
        return $query->getResult();
    }
    
    public function getUserFlow($offset) {
	$em = $this->getDoctrine()->getManager();
	$user = $this->getUser();
        $followingUsers = array();
        
        $following = $em->getRepository('AppBundle:Follower')->findBy(array(
            'follower' => $user,
            'isActive' => true
        ));

	foreach($following as $key => $follow) {
	    $followingUsers[] = $follow->getUser();
	}
        
	$query = $em->createQuery("
	    SELECT p
		FROM AppBundle:Photo p
		   WHERE p.user IN(:users)
			AND p.isModerated = :isModerated
			AND p.isActive = :isActive
			ORDER BY p.creationDate DESC
	")
        ->setParameters(array(
	    'users' => $followingUsers,
	    'isModerated' => true,
	    'isActive' => true
	))
        ->setMaxResults(Photo::FLOW_PHOTOS_LIMIT)
        ->setFirstResult($offset);
        
	return $query->getResult();
    }
    
    public function createFlow($offset, $user) {
	$em = $this->getDoctrine()->getManager();
        $authChecker = $this->get('security.authorization_checker');
        
        if ($authChecker->isGranted('ROLE_USER')) {
            $user = $this->getUser();
        } else {
	    $user = null;
        }
        
	$query = $em->createQuery("
	    SELECT p
		FROM AppBundle:Photo p
		   WHERE p.isModerated = :isModerated
			AND p.isActive = :isActive
                        AND p.creationDate <= :begin
                        AND p.creationDate >= :end
                        AND p.user != :user
                        GROUP BY p.id
			ORDER BY p.moderationDate DESC
	")
        ->setParameters(array(
	    'isModerated' => true,
	    'isActive' => true,
            'begin' => new \DateTime('now'),
            'end' => new \DateTime('-60 days'),
            'user' => $user
	))
        ->setMaxResults(Photo::FLOW_PHOTOS_LIMIT)
        ->setFirstResult($offset);
        
        $photos = $query->getResult();
        
	return $query->getResult();
    }
}
