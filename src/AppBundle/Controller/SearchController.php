<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Search;
use AppBundle\Form\Type\SearchFormType;
use AppBundle\Common\Template;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;

class SearchController extends Controller
{
    public function formAction(Request $request)
    {	
	$form = $this->createForm(SearchFormType::class, new Search());
	
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'form.html.twig'), array('form' => $form->createView()));
    }
    
    public function indexAction(Request $request)
    {	
        $authChecker = $this->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();
	
	$form = $this->createForm(SearchFormType::class, new Search());
	$form->handleRequest($request);
	
	if (!$form->isValid()) {
	    return $this->redirectToRoute('flow');
        }
        
	$phrase = $form['phrase']->getData();

	$userQuery = $em->getRepository('AppBundle:User')->createQueryBuilder('u')
		->where('LOWER(u.username) LIKE :phrase')
		->setParameter('phrase', '%'.strtolower($phrase).'%')
		->getQuery();
	
	$photosQuery = $em->getRepository('AppBundle:Photo')->createQueryBuilder('p')
		->join('p.user', 'u')
		->where('(LOWER(p.title) LIKE :phrase OR LOWER(u.username) LIKE :phrase) AND p.isActive = :isActive AND p.isModerated = :isModerated AND p.moderationDate IS NOT NULL')
		->setParameter('phrase', '%'.strtolower($phrase).'%')
		->setParameter('isActive', true)
		->setParameter('isModerated', true)
		->getQuery();
        
	$users = $userQuery->getResult();
	$photos = $photosQuery->getResult();
        
        if ($authChecker->isGranted('ROLE_USER')) {
            if ($photos) {
                Photo::checkIfPhotosAreLikedByUser($photos, $loggedUser, $em);
            }
            if($users) {
                User::checkIfUsersAreFollowedByUser($users, $loggedUser, $em);
            }
        }
	
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'search.html.twig'), array('users' => $users, 'photos' => $photos, 'phrase' => $phrase));
    }
}

