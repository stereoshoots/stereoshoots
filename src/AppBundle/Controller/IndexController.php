<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\FlowController;
use AppBundle\Common\Template;

class IndexController extends Controller
{
    public function indexAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');
	$em = $this->getDoctrine()->getManager();
        
        if ($authChecker->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('flow');
        }
        
        $formFactory = $this->get('fos_user.registration.form.factory');
	$form = $formFactory->createForm();
        $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();

	$flowController = new FlowController();
	$photographies = $flowController->getPopularPhotographies($em);
	$paintings = $flowController->getPopularPaintings($em);
	$threeDemensionals = $flowController->getPopular3dWorks($em);
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        return $this->render($template->getTemplateByDevice('AppBundle', 'index.html.twig'), array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
	    'photographies' => $photographies,
	    'paintings' => $paintings,
	    'threeDemensionals' => $threeDemensionals
        ));
    }
}

