<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Template;

class ContactController extends Controller
{
    public function indexAction(Request $request)
    {	
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'contact.html.twig'));
    }
}

