<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Template;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;

class DiscoverController extends Controller
{ 
     public function indexAction(Request $request)
    {	
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();
        
        $photos = $this->discoverPhotos($em, $loggedUser);
        $users = $this->discoverUsers($em, $loggedUser);
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
	return $this->render($template->getTemplateByDevice('AppBundle', 'discover.html.twig'), array('photos' => $photos, 'users' => $users));
    }
    
    public function offsetAction(Request $request)
    {
        $loggedUser = $this->getUser();
        
        if ($request->request->get('offset')) {
            $offset = $request->request->get('offset');
        } else {
            $offset = 0;
        }
        
        if (!$request->request->get('type')) {
           throw new Exception('Where is type?');
        }
        
        $type = $request->request->get('type');
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        switch($type) {
            case 'photographies':
                $photos = $this->discoverPhotos($em, $loggedUser, $offset);
                
                return $this->render($template->getTemplateByDevice('AppBundle', 'flowPhoto.html.twig'), array('photos' => $photos));
                break;
            case 'users':
                $users = $this->discoverUsers($em, $offset);
                
                return $this->render($template->getTemplateByDevice('AppBundle', 'flowUser.html.twig'), array('users' => $users));
                break;
            default:
                throw new Exception('Invalid type. Somebody trying to hack us.');
                break;
        }
    }
    
    public function discoverPhotos($em, $user, $offset = 0)
    {
        $query = $em->createQuery("
	    SELECT p
		FROM AppBundle:Photo p
                JOIN AppBundle:Like l  WITH p = l.photo
		   WHERE p.isModerated = :isModerated
			AND p.isActive = :isActive
                        AND p.creationDate <= :begin
                        AND p.creationDate >= :end
                        GROUP BY p.id
			ORDER BY p.moderationDate DESC
	")
        ->setParameters(array(
	    'isModerated' => true,
	    'isActive' => true,
            'begin' => new \DateTime('now'),
            'end' => new \DateTime('-30 days')
	))
        ->setMaxResults(Photo::FLOW_PHOTOS_LIMIT)
        ->setFirstResult($offset);
        
        $photos = $query->getResult();
        
        Photo::checkIfPhotosAreLikedByUser($photos, $user, $em);
        
        return $photos;
    }
    
    public function discoverUsers($em, $loggedUser, $offset = 0)
    {
        $users = $em->getRepository('AppBundle:User')->findBy(array(), null, User::DISCOVER_USERS_LIMIT, $offset);
        
        foreach($users as $key => $user) {
            $follow = $em->getRepository('AppBundle:Follower')->findBy(array(
                'user' => $user,
                'follower' => $loggedUser,
                'isActive' => true
            ));
            
            if ($follow) {
                $user->setFollowed(true);
            }
        }
        
        return $users;
    }
}