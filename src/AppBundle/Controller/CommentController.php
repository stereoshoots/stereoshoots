<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Follower;
use AppBundle\Exception\FollowException;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Comment;
use AppBundle\Exception\PhotoException;
use AppBundle\Exception\CommentException;
use AppBundle\Common\Template;
use AppBundle\Entity\Activity;

class CommentController extends Controller
{ 
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        $commentText = $request->request->get('comment');
        $photoId = $request->request->get('photoId');
        
        if (!$commentText) {
            throw new CommentException('Comment Text not found.', CommentException::PARAMETER_IS_MISSING);
        }
        
        if (!$photoId) {
            throw new CommentException('Photo ID not found.', CommentException::PARAMETER_IS_MISSING);
        }
        
        $photo = $em->getRepository('AppBundle:Photo')->find($photoId);
        
        if (!$photo) {
	    throw new PhotoException('Фотография не найдена', PhotoException::PHOTO_NOT_FOUND);
        }

        $comment = new Comment($user, $photo, $commentText);
        $activity = new Activity(Activity::TYPE_COMMENT, $user, $photo->getUser(), $photo);
        
        $em->persist($comment);
        $em->persist($activity);
        $em->flush();
        
        $template = new Template($this->get('mobile_detect.mobile_detector'));
        
        return $this->render($template->getTemplateByDevice('AppBundle', 'comment.html.twig'), array('comment' => $comment));
    }
    
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $commentId = $request->request->get('commentId');
        
        if (!$commentId) {
            throw new CommentException('Comment ID not found.', CommentException::PARAMETER_IS_MISSING);
        }
        
        $comment = $em->getRepository('AppBundle:Comment')->find($commentId);
        
        if (!$comment) {
            throw new CommentException('Comment not found.', CommentException::COMMENT_NOT_FOUND);
        }
        
        if ($user->getId() != $comment->getUser()->getId()) {
            throw new CommentException('Comment is not yours.', CommentException::COMMENT_IS_NOT_YOURS);
        }
        
        $comment->setActive(false);
        
        $activity = $em->getRepository('AppBundle:Activity')->findOneBy(
            array('user' => $user, 'type' => Activity::TYPE_COMMENT, 'photo' => $comment->getId())
        );
        
        if ($activity) {
            $activity->setIsActive(false);
        }
        
        $em->flush();
        
	return new Response('success');
    }
}