<?php
namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Common\Locale;


class LanguageListener implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        
        // Exclude subrequests
        if(HttpKernel::MASTER_REQUEST != $event->getRequestType())
            return;

        $route = $event->getRequest()->get('_route');
        
        // Exclude assets
        if(strpos($route, "_") === 0)
            return;

        $response = new Response();

        $locale = $request->query->get('lang');
        if(!$locale) {
            $locale = $request->cookies->get('_locale');

            if (!$locale)
                $locale = $locale = Locale::getLocaleByIP($_SERVER['REMOTE_ADDR']);
        }

        //$request->getSession()->set('_locale', $locale);
        $response->headers->setCookie(new Cookie('_locale', $locale));
        $response->send();

        $request->setLocale($locale);
        $request->setDefaultLocale($locale);
        $request->request->set("lang", $locale);
        $request->query->set("lang", $locale);

    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            \Symfony\Component\HttpKernel\KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }
}