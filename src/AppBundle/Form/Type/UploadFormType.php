<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UploadFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
            ->add('name', FileType::class, array('data_class' => null))
            ->add('title', TextType::class)
            ->add('description', TextareaType::class, array('required' => false))
            ->add('category', ChoiceType::class, array(
            'choices' => array(
                'uncategorized' => 1,
                'photography'   => array(
                    'abstract' => 5,
                    'animals'  => 6,
                    'blackandwhite'  => 7,
                    'architecture'  => 8,
                    'fashion'  => 9,
                    'food'  => 10,
                    'landscapes'  => 11,
                    'macro' => 12, 
                    'people' => 13,
                    'sport' => 14
                ),
                'painting' => array(
                    'landscapes' => 15,
                    'seascapes' => 16,
                    'portrait' => 17,
                    'stilllife' => 18,
                    'architecture' => 19,
                    'fantasy' => 20
                 ),
                '3d'   => array(
                    'interiors' => 21,
                    'exteriors' => 22,
                    'creative' => 23
                ),
            ),
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_photo_upload';
    }
}