<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

class LoadCategoryData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
	/*
	 * Root Categories
	 */
        $category = new Category();
        $category->setName('uncategorized');	
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('photography');	
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('painting');	
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('3d');	
        $manager->persist($category);
        $manager->flush();
	
	$categoryPhotography = $manager->getRepository('AppBundle:Category')->findOneByName('photography');
	$categoryPainting = $manager->getRepository('AppBundle:Category')->findOneByName('painting');
	$category3d = $manager->getRepository('AppBundle:Category')->findOneByName('3d');
	
	/*
	 * Photography subcategories
	 */
	$category = new Category();
        $category->setName('abstract');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('animals');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('blackandwhite');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('architecture');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('fashion');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('food');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('landscapes');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('macro');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('people');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('sport');
	$category->setRootCategory($categoryPhotography);
        $manager->persist($category);
        $manager->flush();
	
	/*
	 * Painting subcategories
	 */
	$category = new Category();
        $category->setName('landscapes');
	$category->setRootCategory($categoryPainting);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('seascapes');
	$category->setRootCategory($categoryPainting);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('portrait');
	$category->setRootCategory($categoryPainting);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('stilllife');
	$category->setRootCategory($categoryPainting);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('architecture');
	$category->setRootCategory($categoryPainting);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('fantasy');
	$category->setRootCategory($categoryPainting);
        $manager->persist($category);
        $manager->flush();
	
	/*
	 * 3D subcategories
	 */
	$category = new Category();
        $category->setName('interiors');
	$category->setRootCategory($category3d);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('exteriors');
	$category->setRootCategory($category3d);
        $manager->persist($category);
        $manager->flush();
	
	$category = new Category();
        $category->setName('creative');
	$category->setRootCategory($category3d);
        $manager->persist($category);
        $manager->flush();
    }
}