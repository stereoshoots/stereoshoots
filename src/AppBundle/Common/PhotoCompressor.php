<?php
namespace AppBundle\Common;


class PhotoCompressor
{
    public $image;
    public $imageType;

    public function __construct($filename = null)
    {
        if (!empty($filename))
            $this->load($filename);
    }
    
    public function load($filename)
    {
        $imageInfo = getimagesize($filename);
        $this->imageType = $imageInfo[2];

        if($this->imageType == IMAGETYPE_JPEG)
            $this->image = imagecreatefromjpeg($filename);
        elseif($this->imageType == IMAGETYPE_GIF)
            $this->image = imagecreatefromgif($filename);
        elseif ($this->imageType == IMAGETYPE_PNG)
            $this->image = imagecreatefrompng($filename);
        else
            throw new Exception("The file you're trying to open is not supported");
    }
    
    public function compress()
    {
         if($this->getWidth() > 2500 && $this->getHeight() > 2500)
            $this->scale(50);
         elseif(($this->getWidth() > 2000 && $this->getHeight() > 1500) || ($this->getWidth() > 1500 && $this->getHeight() > 2000))
             $this->scale(75);
    }
    
    public function save($filename, $compression = 90, $imageType = IMAGETYPE_JPEG, $permissions = null)
    {
        if ($imageType == IMAGETYPE_JPEG)
            imagejpeg($this->image,$filename, $compression);
        elseif($imageType == IMAGETYPE_GIF)
            imagegif($this->image,$filename);       
        elseif($imageType == IMAGETYPE_PNG)
            imagepng($this->image,$filename);
        
        if($permissions != null)
            chmod($filename,$permissions);
    }
    
    public function output($imageType=IMAGETYPE_JPEG, $quality = 80)
    {
        if($imageType == IMAGETYPE_JPEG) {
            header("Content-type: image/jpeg");
            imagejpeg($this->image, null, $quality);
        } 
        elseif($imageType == IMAGETYPE_GIF) {
            header("Content-type: image/gif");
            imagegif($this->image);         
        }
        elseif($imageType == IMAGETYPE_PNG) {
            header("Content-type: image/png");
            imagepng($this->image);
        }
    }
    
    public function getWidth()
    {
        return imagesx($this->image);
    }
    
    public function getHeight()
    {
        return imagesy($this->image);
    }
    
    public function resizeToHeight($height)
    {
        $ratio = $height / $this->getHeight();
        $width = round($this->getWidth() * $ratio);
        $this->resize($width,$height);
    }
    
    public function resizeToWidth($width)
    {
        $ratio = $width / $this->getWidth();
        $height = round($this->getHeight() * $ratio);
        $this->resize($width,$height);
    }
    
    public function square($size)
    {
        $newImage = imagecreatetruecolor($size, $size);
        if ($this->getWidth() > $this->getHeight()) {
                $this->resizeToHeight($size);

                imagecolortransparent($newImage, imagecolorallocate($newImage, 0, 0, 0));
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);
                imagecopy($newImage, $this->image, 0, 0, ($this->getWidth() - $size) / 2, 0, $size, $size);
        } else {
                $this->resizeToWidth($size);

                imagecolortransparent($newImage, imagecolorallocate($newImage, 0, 0, 0));
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);
                imagecopy($newImage, $this->image, 0, 0, 0, ($this->getHeight() - $size) / 2, $size, $size);
        }
        $this->image = $newImage;
    }

    public function scale($scale)
    {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getHeight() * $scale / 100; 
        $this->resize($width,$height);
    }

    public function resize($width,$height)
    {
        $newImage = imagecreatetruecolor($width, $height);

        imagecolortransparent($newImage, imagecolorallocate($newImage, 0, 0, 0));
        imagealphablending($newImage, false);
        imagesavealpha($newImage, true);

        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;   
    }
    public function cut($x, $y, $width, $height)
    {
        $newImage = imagecreatetruecolor($width, $height);	
        imagecolortransparent($newImage, imagecolorallocate($newImage, 0, 0, 0));
        imagealphablending($newImage, false);
        imagesavealpha($newImage, true);
        imagecopy($newImage, $this->image, 0, 0, $x, $y, $width, $height);
        $this->image = $newImage;
    }
    
    public function maxarea($width, $height = null)
    {
        $height = $height ? $height : $width;

        if ($this->getWidth() > $width) {
                $this->resizeToWidth($width);
        }
        if ($this->getHeight() > $height) {
                $this->resizeToheight($height);
        }
    }

    public function minarea($width, $height = null)
    {
        $height = $height ? $height : $width;

        if ($this->getWidth() < $width) {
                $this->resizeToWidth($width);
        }
        if ($this->getHeight() < $height) {
                $this->resizeToheight($height);
        }
    }
    
    public function cutFromCenter($width, $height)
    {
        if ($width < $this->getWidth() && $width > $height) {
                $this->resizeToWidth($width);
        }
        if ($height < $this->getHeight() && $width < $height) {
                $this->resizeToHeight($height);
        }

        $x = ($this->getWidth() / 2) - ($width / 2);
        $y = ($this->getHeight() / 2) - ($height / 2);

        return $this->cut($x, $y, $width, $height);
    }
    
    public function maxareafill($width, $height, $red = 0, $green = 0, $blue = 0)
    {
        $this->maxarea($width, $height);
        $newImage = imagecreatetruecolor($width, $height); 
        $colorFill = imagecolorallocate($newImage, $red, $green, $blue);
        
        imagefill($newImage, 0, 0, $colorFill);        
        imagecopyresampled(
                $newImage, 
                $this->image, 
                floor(($width - $this->getWidth())/2), 
                floor(($height-$this->getHeight())/2), 
                0, 
                0, 
                $this->getWidth(), 
                $this->getHeight(), 
                $this->getWidth(), 
                $this->getHeight()
        ); 
        
        $this->image = $newImage;
    }
}