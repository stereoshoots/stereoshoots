<?php
/*
 * Sign class
 * Used to encrypt files with private key
 * @author Tigran Muradyan
 */

namespace AppBundle\Common;

use AppBundle\Common\SignException;

class Sign {
    public
        $prvKey = null,
        $pubKey = null,
        $bInvert = false;

    /*
     * Reverse the stringq
     */
    public function reverseString($str){
            return strrev($str);
    }

    /*
     * Sign string with private key
     */
    public function sign($str){
        if($this->prvKey){
            openssl_sign($str, $sign, $this->prvKey);

            if($this->bInvert)
                $sign = $this->reverseString($sign);

            openssl_free_key($this->prvKey);

            return $sign;
        }
        else
            throw new SignException('No private key', SignException::KEY_NOT_FOUND);
    }

    /*
     * Sign string with private key and return its base64
     */
    public function signBase64AndKey($str){
        return base64_encode($this->sign($str));
    }

    /*
     * Encode string with base64
     */
    public function signBase64($str){
        return base64_encode($str);
    }

    /*
     * Check sign with public key
     */
    public function checkSign($data, $sign, $pubKey){
        if($this->bInvert)
            $sign = $this->reverseString($sign);

        return openssl_verify($data, $sign, $pubKey);
    }

    /*
     * Check Sign with public key
     */
    public function checkSignBase64($data, $sign, $pubKey){
        return $this->checkSign($data, base64_decode($sign), $pubKey);
    }
    
    public function encrypt($data) {
        openssl_public_encrypt($data, $encrypted, $this->pubKey);
        
        return chunk_split(base64_encode($encrypted));
    }
    
    public function decrypt($data) {
        openssl_private_decrypt(base64_decode($data), $out, $this->prvKey);
        
        return $out;
    }
    
    /*
     * Load private key
     */
    public function loadPrivateKey($fileName, $password = null){
        if(!is_file($fileName))
            throw new SignException('Private key not found', SignException::KEY_NOT_FOUND);

        $fileContent = file_get_contents($fileName);
        if(!is_null($password))
            $this->prvKey = openssl_get_privatekey($fileContent, $password);
        else
            $this->prvKey = openssl_get_privatekey($fileContent);

        if(!empty(openssl_error_string()))
            throw new SignException('OpenSSL Error: '.openssl_error_string());

        if(!is_resource($this->prvKey))
            throw new SignException('Private key is not resourse', SignException::EXTERNAL_ERROR);
    }

    /*
     * Load public key
     */
    public function loadPublicKey($fileName){
        if(!is_file($fileName))
            throw new SignException('Public key not found', SignException::KEY_NOT_FOUND);

        $fileContent = file_get_contents($fileName);

        $this->pubKey = openssl_get_publickey($fileContent);

        if(!empty(openssl_error_string()))
            throw new SignException('OpenSSL Error: '.openssl_error_string());

        if(!is_resource($this->pubKey))
            throw new SignException('Public key is not resourse', SignException::EXTERNAL_ERROR);
    }
}