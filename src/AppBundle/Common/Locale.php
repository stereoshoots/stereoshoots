<?php
namespace AppBundle\Common;

class Locale
{
    /**
     * Find out locale by IP Address using TabGeo
     * @param Request $request
     * @return mixed ru | en
     */
    public static function getLocaleByIP($ip){
       $countryCode = \TabGeo\country($ip);

        switch ($countryCode){
            case 'AZ':
            case 'AM':
            case 'BY':
            case 'GE':
            case 'KZ':
            case 'KG':
            case 'MD':
            case 'RU':
            case 'TJ':
            case 'UZ':
            case 'UA':
                return 'ru';
                break;
            default:
                return 'en';
                break;
        }
    }
}
