<?php
namespace AppBundle\Common;

class Template
{
    private $mobileDetector;
    
    public function __construct($mobileDetector)
    {
        $this->mobileDetector = $mobileDetector;
    }
    public function getTemplateByDevice($bundle, $name)
    {
        if($this->mobileDetector->isMobile() || $this->mobileDetector->isTablet())
            $template = $bundle.':Mobile:'.$name;
        else
            $template = $bundle.':Main:'.$name;
        
        return $template;
    }
}
