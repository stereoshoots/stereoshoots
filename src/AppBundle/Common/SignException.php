<?php
namespace AppBundle\Common;

class SignException extends \Exception
{
    const
            KEY_NOT_FOUND = 100,
            EXTERNAL_ERROR = 101;
}
