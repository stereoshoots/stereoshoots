<?php
namespace AppBundle\Exception;

class PhotoEffectException extends \Exception
{
    const
	    PHOTO_NOT_FOUND = 100;
}
