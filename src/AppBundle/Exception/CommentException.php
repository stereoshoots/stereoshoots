<?php
namespace AppBundle\Exception;

class CommentException extends \Exception
{
    const
	    COMMENT_NOT_FOUND = 100,
            COMMENT_IS_NOT_YOURS = 101,
            PARAMETER_IS_MISSING = 102;
}
