<?php
namespace AppBundle\Exception;

class CategoryException extends \Exception
{
    const
	    CATEGORY_DOESNT_EXIST = 100;
}
