<?php
$container->setParameter('database_host', getenv('DATABASE_HOST'));
$container->setParameter('database_port', getenv('DATABASE_PORT'));
$container->setParameter('database_name', getenv('DATABASE_NAME'));
$container->setParameter('database_user', getenv('DATABASE_USER'));
$container->setParameter('database_password', getenv('DATABASE_PASSWORD'));
$container->setParameter('mailer_transport', 'smtp');
$container->setParameter('mailer_host', 'smtp.yandex.ru');
$container->setParameter('mailer_port', '465');
$container->setParameter('mailer_encryption', 'ssl');
$container->setParameter('mailer_user', 'noreply@emotionfolio.com');
$container->setParameter('mailer_password', getenv('MAILER_PASSWORD'));
$container->setParameter('secret', getenv('SECRET'));