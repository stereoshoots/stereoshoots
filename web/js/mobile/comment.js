$('.comment-submit').click(function() {
   $('.add-comment-form').submit(); 
});

$('.add-comment-form').submit(function(e){
    e.preventDefault();

    var comment = $(this).find('#app_photo_comment_content');
    var commentVal = $(comment).val();

    $.ajax({
        type: 'POST',
        url: addCommentUrl,
        data: { photoId : photoId, comment : commentVal },
        success: function(data){
            $('.comments').prepend($(data).fadeIn('slow'));
            $(comment).val('');
        }
    });
});

 $(document).on('click', ".delete-comment", function(e) {
    e.preventDefault();

    var comment = $(this).parent().parent();
        commentId = $(comment).attr('id');

    $.ajax({
        type: 'POST',
        url: deleteCommentUrl,
        data: { commentId : commentId },
        success: function(data){
            if(data == 'success') {
                $(comment).fadeOut();
            }
        }
    });
});