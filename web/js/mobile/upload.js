function showImage(input) {
    if(input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploaded-image').attr('src', e.target.result);
            $('#uploaded-image').fadeIn();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$('.upload-btn').click(function() {
    $('.upload-input').trigger('click');
});

$("#app_photo_upload_name").change(function(){
    $('.upload-wrapper').hide();
    showImage(this);
});