$('.zmdi-favorite').click(function() {
    var btn = $(this),
    photoId = $(this).parent().attr('id'),
    likesCount = parseInt($(this).parent().find('.likes-count').text());

    if(userId) {
        if($(this).hasClass('like')) {
            likesCount += 1;

            $(btn).removeClass('like').addClass('unlike');
            $(btn).parent().find('.likes-count').html(likesCount);
            
            $.ajax({
                type: 'POST',
                url: likeUrl,
                data: { userId: userId, photoId: photoId }
            });
        }
        else if($(this).hasClass('unlike')) {
            likesCount -= 1;

            $(btn).removeClass('unlike').addClass('like');
            $(btn).parent().find('.likes-count').html(likesCount);
            
            $.ajax({
                type: 'POST',
                url: unlikeUrl,
                data: { userId: userId, photoId: photoId }
            });
        }
    }
});