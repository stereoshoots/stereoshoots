$(document).on("click", ".zmdi-close", function() {
    $('#app_search_phrase').val('');
});
    
$(document).on('click', ".btn-subscribe", function() {
    userId = $(this).attr('id');
    
    subscribe(userId, followerId, subscribeUrl)
    
    $(this).removeClass('flowUser-btn-subscribe btn-subscribe').addClass('flowUser-btn-unsubscribe btn-unsubscribe');
});

$(document).on('click', ".btn-unsubscribe", function() {
    userId = $(this).attr('id');

    unsubscribe(userId, followerId, unsubscribeUrl)
    
    $(this).removeClass('flowUser-btn-unsubscribe btn-unsubscribe').addClass('flowUser-btn-subscribe btn-subscribe');
});

$('.search-navbar').click(function() {
    var navbarId = $(this).attr('id');
    if(!$(this).hasClass('search-navbar-active')) {
        $('.search-navbar').removeClass('search-navbar-active');
        $(this).addClass('search-navbar-active');
        
        $('.navbar-content').hide();
        $('#'+ navbarId + '-content').show();
    }
});