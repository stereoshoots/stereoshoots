$(document).on('click', ".btn-subscribe", function() {
    subscribe(userId, followerId, subscribeUrl)

    $(this).removeClass('btn-subscribe btn-primary').addClass('btn-unsubscribe btn-success').text(followingContent);
});

$(document).on('click', ".btn-unsubscribe", function() {
    unsubscribe(userId, followerId, unsubscribeUrl)

    $(this).removeClass('btn-unsubscribe btn-success').addClass('btn-subscribe btn-primary').text(followContent);
});