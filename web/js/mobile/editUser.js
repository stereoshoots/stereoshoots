$('#fos_user_profile_form_wallImage, #fos_user_profile_form_avatar').on('change', function(ev) {
    var imageId = $(this).attr('id'),
        f = ev.target.files[0],
        fr = new FileReader(),
        img = null;
    
    switch (imageId) {
        case 'fos_user_profile_form_wallImage':
            img = 'wallImg';
            break
        case 'fos_user_profile_form_avatar':
            img = 'avatarImg';
            break
        default:
            alert('Undefined');
    }
    
    fr.onload = function(ev2) {
        $('#' + img).attr('src', ev2.target.result);
    };

    fr.readAsDataURL(f);
});