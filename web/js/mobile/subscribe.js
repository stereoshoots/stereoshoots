function subscribe(userId, followerId, url) {
    $.ajax({
        type: 'POST',
        url: url,
        data: { userId: userId, followerId: followerId }
    });
}

function unsubscribe(userId, followerId, url) {
    $.ajax({
        type: 'POST',
        url: url,
        data: { userId: userId, followerId: followerId }
    });
}