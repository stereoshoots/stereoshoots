function showImage(input) {
    if(input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-image').attr('src', e.target.result);
            $('#upload-image').fadeIn();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#app_photo_upload_name").change(function(){
    showImage(this);
    
    $('.upload-input-row input').prependTo('#col-right');
});